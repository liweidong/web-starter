package com.lwd.webservice.impl;


import com.lwd.webservice.TService;
import com.lwd.webservice.mybatis.entity.MVCMybatisDemoUser;
import com.lwd.webservice.mybatis.mapper.master.MVCMybatisDemoUserMapper;
import com.lwd.webservice.GetUserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;



/*
 *
 * @author 李卫东
 * @date 2020/10/27
 */

@Service
public class GetUserInfoServiceImpl implements GetUserInfoService{

    @Autowired
    protected TService tService;

    public GetUserInfoServiceImpl(){
        int x = 1;
    }

    @Autowired
    protected MVCMybatisDemoUserMapper mVCMybatisDemoUserMapper;

    @Override
    public void getUserInfoById(String id, Model model)
    {


        //search by id, get UserInfo
        MVCMybatisDemoUser user = mVCMybatisDemoUserMapper.queryUserInfo(id);
        model.addAttribute("name", user.getId())
                .addAttribute("age", user.getAge())
                .addAttribute("height", user.getHeight())
                .addAttribute("weight", user.getWeight());
    }
}
