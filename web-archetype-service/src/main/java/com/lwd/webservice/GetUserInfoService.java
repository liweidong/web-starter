package com.lwd.webservice;



import org.springframework.ui.Model;


/*
 *
 * @author 李卫东
 * @date 2020/10/27
 */


public interface GetUserInfoService {

    void getUserInfoById(String id, Model model);

}
