package com.lwd.webservice.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * @author 李卫东
 * @date 2021/3/24
 */

@Configuration
@MapperScan(basePackages = {"com.lwd.webservice.mybatis.mapper.slave"}, sqlSessionFactoryRef = "sqlSessionFactorySecondary")
public class MybatisSlaveConfig {

    @Autowired
    @Qualifier("slaveDataSource")
    private DataSource secondaryDataSource ;

    @Bean
    public SqlSessionFactory sqlSessionFactorySecondary() throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean() ;
        factoryBean.setDataSource(secondaryDataSource);//设置数据源
//        factoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath*:secondaryMybatis/*Mapper.xml"));
        return factoryBean.getObject() ;

    }

    @Bean
    public SqlSessionTemplate sqlSessionTemplateSecondary() throws Exception {
        SqlSessionTemplate template = new SqlSessionTemplate(sqlSessionFactorySecondary());
        return template;
    }
}
