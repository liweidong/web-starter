package com.lwd.webservice.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * @author 李卫东
 * @date 2021/3/24
 */

@Configuration
public class DataSourceConfig {

    //@primary默认数据源
    @Primary
    @Bean(name = "masterDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.master")
    public DataSource primaryDataSource(){
        return DataSourceBuilder.create().build() ;
    }

    @Bean(name = "slaveDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.slave")
    public DataSource secondaryDataSource(){
        return DataSourceBuilder.create().build() ;
    }
}
