package com.lwd.webservice.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * @author 李卫东
 * @date 2021/3/20
 */
@Configuration
@MapperScan(basePackages = {"com.lwd.webservice.mybatis.mapper.master"}, sqlSessionFactoryRef = "sqlSessionFactoryPrimary")
public class MybatisMasterConfig {

    @Autowired
    /**
     * Spring的Bean注入配置注解，该注解指定注入的Bean的名称，
     * Spring框架使用byName方式寻找合格的bean，
     * 这样就消除了byType方式产生的歧义
     */
    @Qualifier("masterDataSource")
    private DataSource masterDataSource ;

    @Bean
    @Primary //默认SqlSessionFactory
    public SqlSessionFactory sqlSessionFactoryPrimary() throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean() ;
        factoryBean.setDataSource(masterDataSource);//设置数据源
//        factoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath*:primaryMybatis/*Mapper.xml"));
        return factoryBean.getObject() ;

    }

    @Bean
    public SqlSessionTemplate sqlSessionTemplatePrimary() throws Exception {
        SqlSessionTemplate template = new SqlSessionTemplate(sqlSessionFactoryPrimary()) ;
        return template ;
    }
}
