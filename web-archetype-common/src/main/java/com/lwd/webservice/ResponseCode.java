package com.lwd.webservice;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 李卫东
 * @date 2021/3/22
 */
public enum ResponseCode {
    SUCCESS(0, "success"),
    DEGRADATION(201, "接口降级"),
    MISSING_PAGE(202, "缺页响应"),
    PARAMETER_ERROR(400, "请求参数错误"),
    PERMISSION_ERROR(401, "权限错误"),
    LIMIT_ERROR(403, "限流拒绝"),
    SERVICE_ERROR(500, "后端服务异常"),
    DEPENDENT_SERVICE_ERROR(503, "后端依赖服务异常"),
    DEPENDENT_SERVICE_TIMEOUT(504, "后端依赖服务响应超时异常");

    private int code;
    private String message;

    private static final Map<Integer, ResponseCode> DATA = new HashMap<>();

    static {
        Arrays.stream(ResponseCode.values()).forEach(x -> DATA.put(x.getCode(), x));
    }

    public static ResponseCode getType(int code) {
        return DATA.get(code);
    }

    public int getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }

    ResponseCode(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
