package com.lwd.webservice.cache;

import java.util.Map;

/**
 * @author 李卫东
 * @date 2021/3/24
 */
public interface ILocalCache<K, V> {
    V get(K key);

    void set(K key, V value);

    void cleanCache();

    Map<String, Object> getCacheStats();
}