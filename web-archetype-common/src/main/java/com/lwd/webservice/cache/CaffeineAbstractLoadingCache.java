package com.lwd.webservice.cache;

/**
 * 缓存基础类
 * @author 李卫东
 * @date 2021/3/24
 */

import com.github.benmanes.caffeine.cache.CacheLoader;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.github.benmanes.caffeine.cache.stats.CacheStats;
import com.google.common.base.Throwables;
import lombok.extern.slf4j.Slf4j;
import java.text.NumberFormat;
import java.time.Duration;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
public abstract class CaffeineAbstractLoadingCache<K, V> implements ILocalCache<K, V> {
    private ExecutorService executor = Executors.newSingleThreadExecutor();
    private int maximumSize = 1000;
    private Duration expireAfterWrite = Duration.ofSeconds(60);
    private Duration expireAfterAccess = Duration.ZERO;
    private static final TimeUnit DEFAULT_TIME_UNIT = TimeUnit.MILLISECONDS;
    /**
     * 异步刷新时间，缓存项只有在被检索时才会真正刷新，即只有刷新间隔时间到了你再去get(key)才会重新去执行
     */
    private Duration refreshTime = Duration.ZERO;
    private Date resetTime;
    private long highestSize = 0;
    private Date highestTime;

    private LoadingCache<K, V> cache;

    private final Map<K, Long> failTime = new ConcurrentHashMap<>();

    private static List<CaffeineAbstractLoadingCache> cacheList = new CopyOnWriteArrayList<>();

    /**
     * @return cache
     */
    public LoadingCache<K, V> getCache() {
        if (cache == null) {
            synchronized (this) {
                if (cache == null) {
                    Caffeine<K, V> caffeine = (Caffeine<K, V>) Caffeine.newBuilder();
                    caffeine.maximumSize(maximumSize)
                            .expireAfterWrite(expireAfterWrite.toMillis(), DEFAULT_TIME_UNIT)
                            .recordStats();
                    if (expireAfterAccess.compareTo(Duration.ZERO) > 0) {
                        caffeine.expireAfterAccess(expireAfterAccess.toMillis(), DEFAULT_TIME_UNIT);
                    }
                    if (refreshTime.compareTo(Duration.ZERO) > 0) {
                        CacheLoader<K, V> loader = getKvCacheLoader();
                        cache = caffeine.refreshAfterWrite(refreshTime.toMillis(), DEFAULT_TIME_UNIT)
                                .executor(executor)
                                .build(loader);
                    } else {
                        cache = caffeine.build(this::fetchData);
                    }
                    cacheList.add(this);
                    this.resetTime = new Date();
                    this.highestTime = new Date();
                    log.debug("本地缓存初始化成功");
                }
            }
        }

        return cache;
    }

    private CacheLoader<K, V> getKvCacheLoader() {
        CacheLoader<K, V> loader = new CacheLoader<K, V>() {
            @Override
            public V load(K key) throws Exception {
                return fetchData(key);
            }

            @Override
            public CompletableFuture<V> asyncReload(K key, V oldValue, Executor executor) {
                return CompletableFuture.supplyAsync(() -> {
                    V obj = null;
                    Long keyLastFailedTime = failTime.get(key);
                    //如果上次失败还在30秒内不再重试
                    if (keyLastFailedTime == null || (System.currentTimeMillis() - keyLastFailedTime > 30000)) {
                        try {
                            obj = fetchData(key);
                        } catch (Exception e) {
                            failTime.put(key, System.currentTimeMillis());
                            throw new RuntimeException("exception when relad from fetchFata" + key, e);
                        }
                        if (obj == null) {
                            failTime.put(key, System.currentTimeMillis());
                        } else {
                            failTime.remove(key);
                        }
                    }
                    return obj;
                }, executor);
            }
        };
        return loader;
    }


    /**
     * @param key
     * @return Value
     */
    protected V getValue(K key) {
        V result = null;
        result = getCache().get(key);
        if (getCache().estimatedSize() > highestSize) {
            highestSize = getCache().estimatedSize();
            highestTime = new Date();
        }
        return result;
    }

    public Map<K, V> getValues(Iterable<? extends K> keys) {
        Map<K, V> result = getCache().getAll(keys);
        if (getCache().estimatedSize() > highestSize) {
            highestSize = getCache().estimatedSize();
            highestTime = new Date();
        }
        return result;
    }

    protected void setValue(K key, V value) {
        LoadingCache<K, V> cache = getCache();
        cache.put(key, value);
    }

    protected abstract V fetchData(K key);

    protected void cleanCache(K key) {
        LoadingCache<K, V> cache = getCache();
        cache.invalidate(key);
    }

    protected void cleanCacheAll() {
        LoadingCache<K, V> cache = getCache();
        cache.invalidateAll();
    }

    protected void refreshKey(K key) {
        LoadingCache<K, V> cache = getCache();
        cache.refresh(key);
    }

    protected boolean containsKey(K key) {
        LoadingCache<K, V> cache = getCache();
        return cache.asMap().containsKey(key);
    }

    public void refreshKey(String channel, String entityId) {
        if (isInterested(channel, entityId)) {
            K key = fetchKeyFromMessage(channel, entityId);
            if (key != null) {
                refreshKey(key);
            }
        }
    }

    protected boolean isInterested(String channel, String entityId) {
        return false;
    }

    protected K fetchKeyFromMessage(String channel, String entityId) {
        return null;
    }

    public long getHighestSize() {
        return highestSize;
    }

    public Date getHighestTime() {
        return highestTime;
    }

    public Date getResetTime() {
        return resetTime;
    }

    public void setResetTime(Date resetTime) {
        this.resetTime = resetTime;
    }

    public int getMaximumSize() {
        return maximumSize;
    }

    public long getEstimatedSize() {
        LoadingCache<K, V> cache = getCache();
        return cache.estimatedSize();
    }

    public Duration getExpireAfterWrite() {
        return expireAfterWrite;
    }

    public Duration getRefreshTime() {
        return refreshTime;
    }

    public void setRefreshTime(Duration refreshTime) {
        this.refreshTime = refreshTime;
    }

    public void setMaximumSize(int maximumSize) {
        this.maximumSize = maximumSize;
    }

    public void setExpireAfterWrite(Duration expireAfterWrite) {
        this.expireAfterWrite = expireAfterWrite;
    }

    public void setExpireAfterAccess(Duration expireAfterAccess) {
        this.expireAfterAccess = expireAfterAccess;
    }

    public static List<CaffeineAbstractLoadingCache> getCacheList() {
        return cacheList;
    }


    @Override
    public void set(K key, V value) {
        try {
            setValue(key, value);
        } catch (Exception e) {
            log.error("设置缓存失败[{}].set({}) exception:{}", this.getClass().getSimpleName(), key, Throwables.getStackTraceAsString(e));
        }
    }

    @Override
    public void cleanCache() {
        try {
            cleanCacheAll();
        } catch (Exception e) {
            log.error("清除缓存失败", e);
        }
    }

    @Override
    public Map<String, Object> getCacheStats() {
        String cacheName = this.getClass().getSimpleName();
        try {
            Map<String, Object> map = new LinkedHashMap<>();
            LoadingCache<K, V> cache = getCache();
            CacheStats cs = cache.stats();
            // 建立百分比格式化用
            NumberFormat percent = NumberFormat.getPercentInstance();
            // 百分比小数点后的位数
            percent.setMaximumFractionDigits(1);
            map.put("cacheName", cacheName);
            map.put("size", cache.estimatedSize());
            map.put("hitCount", cs.hitCount());
            map.put("hitRate", percent.format(cs.hitRate()));
            map.put("missRate", percent.format(cs.missRate()));
            map.put("loadSuccessCount", cs.loadSuccessCount());
            map.put("loadFailureCount", cs.loadFailureCount());
            map.put("loadExceptionCount", cs.loadFailureCount());
            //ms
            map.put("totalLoadTime", cs.totalLoadTime() / 1000000);
            map.put("maximumSize", this.getMaximumSize());
            map.put("refreshTime", this.getRefreshTime());
            map.put("expireAfterAccess", this.expireAfterAccess);
            map.put("expireAfterWrite", this.getExpireAfterWrite());
            map.put("usedCapacityPercent", (cache.estimatedSize() * 100 / this.getMaximumSize()) + "%");
            return map;
        } catch (Exception e) {
            log.error("[{}].getCacheStats exception:{}", cacheName, Throwables.getStackTraceAsString(e));
        }
        return null;
    }
}